const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
const question = (question) =>
  new Promise((resolve) => {
    rl.question(question, (answer) => {
      resolve(answer);
    });
  });
(async () => {
  const val1 = await question('Enter first number: ');
  const val2 = await question('Enter second number: ');
  rl.close();
  console.log(val1 * val2);
})();