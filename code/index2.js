// const fs = require('fs');
// const http = require('http');

// // Promise.resolve()
// //     .then(() => {
// //         console.log('promise 1')
// //     })
// fs.readFile('./file.txt', 'utf-8', (err, result) => {
//     console.log(result)

//     // setTimeout( () => {
//     //     console.log('st 0 in')
//     // },0)

//     // Promise.resolve()
//     // .then(() => {
//     //     console.log('promise 1 in')
//     // })




//     // setImmediate( () => {
//     //     console.log('immediate 1 in')
//     // });
// })




// setTimeout( () => {
//     console.log('st 0')
// },0)

// setTimeout( () => {
//     console.log('st 6')
// },6000)

// setImmediate( () => {
//     console.log('immediate 1')
// });









// Promise.resolve()
//     .then(() => {
//         console.log('promise 1')
//     })
// http.get('http://nodejs.org/dist/index.json', () => {
    // console.log('http')

    // setTimeout( () => {
    //     console.log('st 0 in')
    // },0)

    // Promise.resolve()
    // .then(() => {
    //     console.log('promise 1 in')
    // })




    // setImmediate( () => {
    //     console.log('immediate 1 in')
    // });
// })



// const now = Date.now();
    // while (Date.now() - now <= 5000);


// const fs = require('fs');
// fs.readFile('./file.txt', 'utf-8', (err, data) => {
//     let arr = data.toString().split("\n")
//     console.log(...data)
// })
// fs.writeFile('test2', 'qwrtrqw',(e) => {
//     console.log(e)
// })

// fs.readFile('./test2', 'utf-8', (err, data) => {
//     console.log(data)
// })

const { Worker, isMainThread,  workerData } = require('worker_threads');

let currentVal = 0;
let intervals = [100,1000, 500]

function counter(id, i){
    console.log("[", id, "]", i)
    return i;
}

if(isMainThread) {
    console.log("this is the main thread")
    for(let i = 0; i < 2; i++) {
        let w = new Worker(__filename, {workerData: i});
    }

    setInterval((a) => currentVal = counter(a,currentVal + 1), intervals[2], "MainThread");
} else {

    console.log("this isn't")

    setInterval((a) => currentVal = counter(a,currentVal + 1), intervals[workerData], workerData);

}
console.log('hello world')
