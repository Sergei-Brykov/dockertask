const fs = require('fs')
const flag = process.argv[2]
const p = require('path')


const path = p.resolve(__dirname ,process.argv[3])
const statsMain = fs.statSync(path || './')

if (!(flag && path)) {
    let result = fs.readdirSync('./')
    console.log(result.join('\t'))
} 

if (flag === '-a'){
    let result = '.\t..\t'
    if (statsMain.isDirectory()){
        fs.readdirSync(path).forEach((file) => {
            result += `${file.toString()}\t`
        })
    } else {
        result += path
    }
    console.log(result)
}
if (flag === '-l'){
    if (statsMain.isDirectory()) {
        let array = fs.readdirSync(path)
        for (i = 0; i < array.length; i++){
            getInfo(array[i])
        }
    } else {
        getInfo(path)
    }
}
if (flag === '-lh'){
    if (statsMain.isDirectory()) {
        let array = fs.readdirSync(path)
        for (i = 0; i < array.length; i++){
            getInfo(array[i], true)
        }
    } else {
        getInfo(path, true)
    }
}   
function getInfo(file, ifConandH) {
    let result =''
    
    const stats = fs.statSync((path + '/' + file).toString())
    let type
    if (stats.isDirectory()){
        type = 'd'
    } else if(stats.isSymbolicLink() ) {
        type = 'l'
    } else {
        type = '-'
    }
    const rights =  (stats['mode'] & 400 ? 'r' : '-')+
    (stats['mode'] & 200 ? 'w' : '-')+
    (stats['mode'] & 100 ? 'x' : '-')+
    (stats['mode'] & 40 ? 'r' : '-')+
    (stats['mode'] & 20 ? 'w' : '-')+
    (stats['mode'] & 10 ? 'x' : '-')+
    (stats['mode'] & 4 ? 'r' : '-')+
    (stats['mode'] & 2 ? 'w' : '-')+
    (stats['mode'] & 1 ? 'x' : '-')
    const month = stats.ctime.toLocaleString('ru', {
        month: 'short'
    }).match(/[^.]+/)
    const minute = stats.ctime.getMinutes() < 10 ? `0${stats.ctime.getMinutes()}` : stats.ctime.getMinutes()
    const date = `${month} ${stats.ctime.getHours()}:${minute}`
    const user = stats.uid
    const group = stats.gid
    let size
    if (ifConandH) {
        if (stats.size / 1024 < 1) {
                size = stats.size
            } else {
                size = Math.floor(stats.size / 1024) + 'k'
            }
        } else size = stats.size
    result = `${type}${rights} ${stats.nlink} ${stats.uid} ${stats.gid}\t ${size}\t ${date} ${file}`
    console.log(result)
}

