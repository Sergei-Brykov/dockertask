const fs = require('fs');
const childProcess = require('child_process');
const readline = require('readline');
const path = require('path')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
const gitLink = process.argv[process.argv.length - 1]
const option = {
    httpUser: null,
    httpPas: null,

    dir: null,
    branch: null,
    gitUserName: null,
    gitUserEmail: null,

    sshKeyPath: null
}
for (i = 2; i < process.argv.length - 1; i++) {
    parseOptions(process.argv[i]);
}
function parseOptions(flag) {
    if (flag.slice(0, 12) == '--http-user=') {
        option.httpUser = flag.slice(12, flag.length)
    } else if (flag.slice(0, 12) == '--http-pass=' ) {
        option.httpPas = flag.slice(12, flag.length)
    } else if (flag.slice(0, 6) == '--dir=' ) {
        option.dir = path.join(flag.slice(6, flag.length)) 
    } else if (flag.slice(0, 9) == '--branch=' ) {
        option.branch = flag.slice(9, flag.length)
    } else if (flag.slice(0, 16) == '--git-user-name=' ) {
        option.gitUserName = flag.slice(16, flag.length)
    } else if (flag.slice(0, 17) == '--git-user-email=' ) {
        option.gitUserEmail = flag.slice(17, flag.length)
    } else if (flag.slice(0, 15) == '--ssh-key-path=' ) {
        option.sshKeyPath = path.join(flag.slice(15, flag.length))
    }
} 
console.log(option)
checkMainOption()
function question(message, property, cb, defolt) {
    rl.question(message, (answer) => {
        option[property] = answer || defolt
        cb()
    })
}
function cloneGitHttp () {
    let Link = gitLink.slice(0, 8) + option.httpUser + ':' + option.httpPas + '@' + gitLink.slice(8)
    childProcess.exec(`git clone -b ${option.branch}  ${Link} ${option.dir}`, function (err, stdout, stderr) {
        if(err) {
            console.log(err.toString())
        }
        console.log(`${stdout}`)
        console.log(`${stderr}`)
        const comand = `cd ${option.dir}; 
        git config user.name ${option.gitUserName}; 
        git config user.email ${option.gitUserEmail}\n
        `
        gitConfig(comand)
    })
}
function gitConfig(comand) {
    childProcess.exec(comand, (err, stdout, stderr) => {
        if (err) {
            console.log(err.toString())
        }
        console.log(`${stdout}`)
        console.log(`${stderr}`)
        rl.close()
    })
}
function checkMainOption() {
    if (!option.dir) {
        question('Plese print directory`s path (defolt="/home/developer/Desktop/proo"): ', 'dir', checkMainOption, "/home/developer/Desktop/proo")
        return
    }
    if (!option.branch) {
        question('Plese prompt git bransh (defolt="master"): ', `branch`, checkMainOption, "master")
        return
    }
    if (!option.gitUserName) {
        question('Plese prompt git user name (defolt="developer"): ', `gitUserName`, checkMainOption, "developer")
        return
    }
    if (!option.gitUserEmail) {
        question('Plese prompt git user email (defolt="email"): ', `gitUserEmail`, checkMainOption, "email")
        return
    }
    checkHttpOrSSH()
}
function checkHttpOrSSH() {
    if (gitLink.slice(0, 5) == 'https') {
        checkHttpOption()
    } else if (gitLink.slice(0, 4) === 'git@') {
        checkSshOption()
    }
}
function checkHttpOption() {
    if (!option.httpUser) {
        question('Plese print User Name: ', 'httpUser', checkHttpOption, null)
        return
    }
    if (!option.httpPas) {
        question('Plese prompt Password: ', `httpPas`, checkHttpOption, null)
        return
    }
    cloneGitHttp()
}
function checkSshOption() {
    if (!option.sshKeyPath) {
        console.log('Defolt copy in ~/.ssh/id_rsa')
        let link = path.join('~/.ssh/id_rsa')
        console.log('here')
        cloneGitSsh(link)
    } else {        
        console.log(fs.existsSync(option.sshKeyPath))
        if (fs.existsSync(option.sshKeyPath)) {
            cloneGitSsh(option.sshKeyPath)
        } else {
            console.log('1')
            createSshKey(option.sshKeyPath)
        }
    }
}
function cloneGitSsh(sshPath) {
    let comand = `bash -c 'GIT_SSH_COMMAND="ssh -i ${sshPath} -F /dev/null" git clone -b ${option.branch}  ${gitLink} ${option.dir}'`
    childProcess.exec(comand, function (err, stdout, stderr) {
        if(err) {
            console.log(err.toString())
        }
        console.log(`${stdout}`)
        console.log(`${stderr}`)
        if (err) {
            catSshKey(sshPath)
            return
        }
        const comand = `cd ${option.dir}; 
        git config user.name ${option.gitUserName}; 
        git config user.email ${option.gitUserEmail}\n
        git config core.sshCommand "ssh -i ${sshPath} -F /dev/null"
        `
        gitConfig(comand)
    })
}
function createSshKey (sshPath) {
    let comand = `ssh-keygen -t rsa -b 2048 -f ${sshPath}`
    childProcess.exec(comand, function (err, stdout, stderr) {
        if(err) {
            console.log(err.toString())
        }
        console.log(`${stdout}`)
        console.log(`${stderr}`)
        catSshKey(sshPath)
    })
}
function catSshKey(sshPath) {
    let pubPath = sshPath + '.pub'
    childProcess.exec(`cat ${pubPath}`, function (err, stdout, stderr) {
        if (err) {
            console.log(err.toString())
        }
        console.log(`${stdout}`)
        console.log(`${stderr}`)
        questionSsh()
    })
}
function questionSsh () {
    rl.question(`Please copy the public key to your git and only then tab enter: `, (answer) => {
        cloneGitSsh(option.sshKeyPath)
    })
}   
