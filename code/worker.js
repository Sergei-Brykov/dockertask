const {Worker, isMainThread, parentPort, workerData} = require('worker_threads')

if (isMainThread) {
  let i = 5
  const worker = new Worker(__filename, { workerData: 1 })
  worker.on('message', msg => {
    console.log(msg)
  })
  worker.on('error', e => {
    console.log(e)
  })
} else {
  parentPort.postMessage(`Hello world from worker ${workerData}`)
}