build:
	docker build -t my_node ./docker/node/
up:
	docker run -d -v $(PWD)/code/:/usr/src/app --name node1 -p 3000:3000 my_node
shell:
	docker exec -it --user="1000" node1 bash